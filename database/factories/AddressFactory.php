<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    protected $model = Address::class;

    public function definition()
    {
        return [
            'district'   => $this->faker->city(),
            'street'     => $this->faker->streetName(),
            'city'       => $this->faker->city(),
            'state'      => $this->faker->country(),
            'post_code'  => $this->faker->numerify('########'),
            'complement' => $this->faker->text(100),
            'number'     => $this->faker->numerify(),
            'patient_id' => User::factory()->create()->getKey(),
        ];
    }
}
