<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    protected $model = Patient::class;

    public function definition()
    {
        return [
            'name'        => $this->faker->name(),
            'mother_name' => $this->faker->monthName(),
            'birth_date'  => $this->faker->dateTimeBetween('-30 years', '-20 years')->format('d-m-Y'),
            'cpf'         => $this->faker->numerify('###.###.###-##'),
            'cns'         => 175429393770004,
        ];
    }
}
