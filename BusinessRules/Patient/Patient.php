<?php

namespace BusinessRules\Patient;

use App\Http\Requests\StorePatientRequest as StoreRequest;
use App\Http\Requests\UpdatePatientRequest as UpdateRequest;
use App\Models\Address;
use App\Models\Image;
use App\Models\Patient as ModelPatient;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Patient
{
    public function registerPatient(StoreRequest $request): JsonResponse
    {
        $error = DB::transaction(function () use ($request) {
            $patient = ModelPatient::create($request->only('name', 'mother_name', 'birth_date', 'cpf', 'cns'));
            $this->patientImageSave($request->file('image', null), $patient);
            $this->patientAddresSave(
                $request->only('street', 'number', 'district', 'city', 'state', 'post_code', 'complement'),
                $patient
            );
        });

        return $error
            ? response()->json(['error' => 'Patient register fail.'], 400)
            : response()->json(['message' => 'Patient register successfully.'], 201);
    }

    public function updatePatient(UpdateRequest $request, ModelPatient $patient): JsonResponse
    {
        $error = DB::transaction(function () use ($request, $patient) {
            $patient->update($request->only('name', 'mother_name', 'birth_date', 'cpf', 'cns'));
            $this->patientImageSave($request->file('image', null), $patient);
            $this->patientAddresSave(
                $request->only('street', 'number', 'district', 'city', 'state', 'post_code', 'complement'),
                $patient
            );
        });

        return $error
            ? response()->json(['error' => 'Patient update fail.'], 400)
            : response()->json(['message' => 'Patient update successfully.'], 200);
    }


    private function patientImageSave(object $patientImageData = null, ModelPatient $patient): void
    {
        if ($patientImageData) {
            if ($patient->image()->exists()) {
                Storage::delete($patient->image->file);
            }
            $path = Storage::disk('public')->put('images', $patientImageData);
            Image::updateOrCreate(
                ['patient_id' => $patient->getKey()],
                ['path'       => $path]
            );
        }
    }

    private function patientAddresSave(array $patientAddresData, ModelPatient $patient): void
    {
        Address::updateOrCreate(
            ['patient_id' => $patient->getKey()],
            $patientAddresData
        );
    }
}
