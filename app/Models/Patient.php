<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    protected $table = 'patients';

    protected $fillable = [
        'name',
        'mother_name',
        'birth_date',
        'cpf',
        'cns',
    ];

    public function getBirthDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d-m-Y');
    }
    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'patient_id', 'id');
    }

    public function image()
    {
        return $this->hasOne(Image::class, 'patient_id', 'id');
    }
}
