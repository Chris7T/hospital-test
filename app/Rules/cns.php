<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class cns implements Rule
{
    public function passes($attribute, $value)
    {
        return $this->cnsValidation789($value) ?? $this->cnsValidation12($value);
    }

    public function message()
    {
        return 'The cns is invalid.';
    }

    private function cnsValidation12($cns)
    {
        $pis = str_split($cns, 11)[0];
        $sum = $this->sumDigits($cns, 5);
        $dv = 11 - ($sum % 11);
        if ($dv == 11) {
            $dv = 0;
        }
        if ($dv == 10) {
            $sum = $this->sumDigits($cns, 5, 2);
            $dv = 11 - ($sum % 11);
            $result = $pis . '001' . strval($dv);
        } else {
            $result = $pis . '000' . strval($dv);
        }

        return ($cns == $result)
            ? true
            : false;
    }

    private function cnsValidation789($cns)
    {
        $sum = $this->sumDigits($cns);
        if ($sum % 11 != 0) {
            return false;
        }
        return true;
    }

    private function sumDigits($cns, $stop = null, $add = 0)
    {
        $count = 15;
        $sum = 0;
        foreach (str_split($cns) as $digit) {
            if ($count == $stop - 1) {
                break;
            }
            $sum += (int)$digit * $count--;
        }
        $sum += $add;
        return $sum;
    }
}
