<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserLoginRequest as LoginRequest;
use App\Http\Requests\User\UserRegisterRequest as RegisterRequest;
use BusinessRules\User\UserAuth as RuleUserAuth;

class UserController extends Controller
{
    private RuleUserAuth $regrasUserAuth;

    public function __construct(RuleUserAuth $regrasUserAuth)
    {
        $this->regrasUserAuth = $regrasUserAuth;
    }

    /**
     * Register new user
     *
     * Register new user
     * @group User
     * @responseFile 422 ApiResponse/UserController/RegisterValidation.json
     * @response 201 {"message": "User Registred."}
     */
    public function register(RegisterRequest $request)
    {
        return $this->regrasUserAuth->register($request);
    }

    /**
     * User login
     *
     * User login
     * @group User
     * @responseFile 422 ApiResponse/UserController/LoginValidation.json
     * @response 200 {"token": "13|HfI40OFYLjWEahpM4QgWEvdqbXbVRpPIelNehKq0"}
     */
    public function login(LoginRequest $request)
    {
        return $this->regrasUserAuth->login($request);
    }

    /**
     * User login
     *
     * User login
     * @group User
     * @responseFile 422 ApiResponse/UserController/LoginValidation.json
     * @response 200 {"token": "13|HfI40OFYLjWEahpM4QgWEvdqbXbVRpPIelNehKq0"}
     */
    public function logout()
    {
        return $this->regrasUserAuth->logout();
    }
}
