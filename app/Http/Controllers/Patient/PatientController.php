<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePatientRequest as StoreRequest;
use App\Http\Requests\UpdatePatientRequest as UpdateRequest;
use App\Http\Resources\Patient\CompletePatientResource as CompleteResource;
use App\Http\Resources\Patient\SimplePatientResource as SimpleResource;
use BusinessRules\Patient\Patient as PatientRules;
use App\Models\Patient;

class PatientController extends Controller
{
    private PatientRules $patientRules;

    public function __construct(PatientRules $patientRules)
    {
        $this->patientRules = $patientRules;
    }

    /**
     * List of Patient
     *
     * Return all patient register 
     * @group Patient
     * @responseFile 200 ApiResponse/PatientController/List.json
     */
    public function index()
    {
        return SimpleResource::collection((Patient::all()));
    }

    /**
     * Register new patient
     *
     * Register new patient
     * @group Patient
     * @responseFile 422 ApiResponse/PatientController/RegisterValidation.json
     * @response 201 {"message": "Patient register successfully."}
     */
    public function store(StoreRequest $request)
    {
        return $this->patientRules->registerPatient($request);
    }

    /**
     * Show patient
     *
     * Return patient data
     * @group Patient
     * @urlParam id integer required O register id.
     * @responseFile ApiResponse/PatientController/Show.json
     * @response 404 {"message": "No query results for model [App\\Models\\Patient]"}
     */
    public function show(Patient $patient)
    {
        return new CompleteResource($patient);
    }

    /**
     * Update Patient
     *
     * Update patient data
     * @group Patient
     * @urlParam id integer required O register id.
     * @responseFile ApiResponse/PatientController/Show.json
     * @responseFile 422 ApiResponse/PatientController/UpdateValidation.json
     * @response 404 {"message": "No query results for model [App\\Models\\Patient]"}
     */
    public function update(UpdateRequest $request, Patient $patient)
    {
        return $this->patientRules->updatePatient($request, $patient);
    }

    /**
     * Delete patient
     *
     * Delete patient data
     * @group Patient
     * @urlParam id integer required O register id.
     * @response 200 {"message": "OK"}
     * @response 404 {"message": "No query results for model [App\\Models\\Patient]"}
     */
    public function destroy(Patient $patient)
    {
        $patient->delete();
        return response()->json(['message' => 'Patient delete successfully.']);
    }
}
