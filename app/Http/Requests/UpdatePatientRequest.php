<?php

namespace App\Http\Requests;

use App\Rules\cns;

class UpdatePatientRequest extends PatientRequest
{
    public function rules()
    {
        return [
            'name'        => ['filled', 'string', 'max:100'],
            'mother_name' => ['filled', 'string', 'max:100'],
            'birth_date'  => ['filled', 'date_format:d-m-Y'],
            'cpf'         => ['filled', 'string', 'regex:/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/'],
            'cns'         => ['filled', 'numeric', 'digits:15', new cns],
            'street'      => ['filled', 'string', 'max:100'],
            'number'      => ['filled', 'numeric'],
            'district'    => ['filled', 'string', 'max:100'],
            'city'        => ['filled', 'string', 'max:100'],
            'state'       => ['filled', 'string', 'max:100'],
            'post_code'   => ['filled', 'numeric', 'digits:8'],
            'complement'  => ['filled', 'string', 'max:100'],
            'path'        => ['nullable', 'image', 'mimes:jpeg,png,jpg,gif,svg']
        ];
    }
}
