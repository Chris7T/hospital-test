<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function bodyParameters(): array
    {
        return [
            'name' => [
                'description' => 'User name.',
                'example'     => 'Pedro Paulo'
            ],
            'mother_name' => [
                'description' => 'User email.',
                'example'     => 'email@email.com'
            ],
            'birth_date' => [
                'description' => 'User password.',
                'example'     => 'password@@@'
            ],
            'cpf' => [
                'description' => 'User cpf.',
                'example'     => '123.456.789-01'
            ],
            'cns' => [
                'description' => 'User cns.',
                'example'     => '99911111000000'
            ],
            'street' => [
                'description' => 'User street.',
                'example'     => 'Street One'
            ],
            'number' => [
                'description' => 'User number.',
                'example'     => '154'
            ],
            'district' => [
                'description' => 'User district.',
                'example'     => 'Alphonso Davies'
            ],
            'city' => [
                'description' => 'User city.',
                'example'     => 'Manhattan'
            ],
            'state' => [
                'description' => 'User state.',
                'example'     => 'New York'
            ],
            'post_code' => [
                'description' => 'User post_code.',
                'example'     => '39400-346'
            ],
            'complement' => [
                'description' => 'User complement.',
                'example'     => 'In front of Pedros Restaurant'
            ],
            'image' => [
                'description' => 'User image.',
                'example' => __DIR__ . '/ImageExample/05CVZQjhQcUXb1KG9efr6j3AHt4lGzPwejni08ME.jpg'
            ],

        ];
    }
}
