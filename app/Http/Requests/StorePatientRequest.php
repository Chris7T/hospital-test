<?php

namespace App\Http\Requests;

use App\Rules\cns;

class StorePatientRequest extends PatientRequest
{
    public function rules()
    {
        return [
            'name'        => ['required', 'string', 'max:100'],
            'mother_name' => ['required', 'string', 'max:100'],
            'birth_date'  => ['required', 'date_format:d-m-Y'],
            'cpf'         => ['required', 'string', 'regex:/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/'],
            'cns'         => ['required', 'numeric', 'digits:15', new cns],
            'street'      => ['required', 'string', 'max:100'],
            'number'      => ['required', 'numeric'],
            'district'    => ['required', 'string', 'max:100'],
            'city'        => ['required', 'string', 'max:100'],
            'state'       => ['required', 'string', 'max:100'],
            'post_code'   => ['required', 'numeric', 'digits:8'],
            'complement'  => ['filled', 'string', 'max:100'],
            'path'        => ['nullable', 'image', 'mimes:jpeg,png,jpg,gif,svg']
        ];
    }
}
