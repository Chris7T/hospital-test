<?php

namespace App\Http\Resources\Patient;

use Illuminate\Http\Resources\Json\JsonResource;

class SimplePatientResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'          => $this->getKey(),
            'name'        => $this->name,
            'mother_name' => $this->mother_name,
            'birth_date'  => $this->birth_date
        ];
    }
}
