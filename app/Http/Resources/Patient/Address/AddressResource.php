<?php

namespace App\Http\Resources\Patient\Address;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'         => $this->getKey(),
            'number'     => $this->number,
            'street'     => $this->street,
            'district'   => $this->district,
            'city'       => $this->city,
            'state'      => $this->state,
            'post_code'  => $this->post_code,
            'complement' => $this->complement,
        ];
    }
}
