<?php

namespace App\Http\Resources\Patient;

use App\Http\Resources\Patient\Address\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompletePatientResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'          => $this->getKey(),
            'name'        => $this->name,
            'mother_name' => $this->mother_name,
            'birth_date'  => $this->birth_date,
            'cpf'         => $this->cpf,
            'cns'         => $this->cns,
            'address'     => new AddressResource($this->address),
            'image_url'   => $this->image?->file
        ];
    }
}
