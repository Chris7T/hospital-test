<?php

use Illuminate\Support\Facades\Route;


Route::apiResource('patient', 'PatientController')->middleware('auth:sanctum');
