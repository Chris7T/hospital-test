<?php

namespace Tests\Feature\Patient;

use App\Models\Address;
use App\Models\Patient;
use App\Models\User;

use Tests\TestCase;

class ShowTest extends TestCase
{
    private const ROTA = 'patient.show';
    private const INVALID_ID = 0;
    private User $user;
    private int $patientId;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->patientId = Patient::factory()->create()->getKey();
        Address::factory()->create(['patient_id' => $this->patientId]);
    }

    public function testUserNotAuth()
    {
        $response = $this->getJson(route(self::ROTA, $this->patientId));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFailInvalidPatient()
    {
        $response = $this->actingAs($this->user)->getJson(route(self::ROTA, self::INVALID_ID));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucess()
    {
        $response = $this->actingAs($this->user)->getJson(route(self::ROTA, $this->patientId));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'mother_name',
                    'birth_date',
                    'cpf',
                    'cns',
                    'address' => [
                        'id',
                        'number',
                        'street',
                        'district',
                        'city',
                        'state',
                        'post_code',
                        'complement'
                    ],
                    'image_url'
                ]
            ]);
    }
}
