<?php

namespace Tests\Feature\Patient;

use App\Models\Address;
use App\Models\Patient;
use App\Models\User;

use Tests\TestCase;

class DeleteTest extends TestCase
{
    private const ROTA = 'patient.destroy';
    private const INVALID_ID = 0;
    private User $user;
    private int $patientId;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->patientId = Patient::factory()->create()->getKey();
        Address::factory()->create(['patient_id' => $this->patientId]);
    }

    public function testUserNotAuth()
    {
        $response = $this->deleteJson(route(self::ROTA, $this->patientId));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFailInvalidPatient()
    {
        $response = $this->actingAs($this->user)->deleteJson(route(self::ROTA, self::INVALID_ID));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucess()
    {
        $response = $this->actingAs($this->user)->deleteJson(route(self::ROTA, $this->patientId));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
}
