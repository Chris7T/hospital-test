<?php

namespace Tests\Feature\Patient;

use App\Models\Address;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

use Tests\TestCase;

class StoreTest extends TestCase
{
    private const ROTA = 'patient.store';
    private User $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    public function testUserNotAuth()
    {
        $response = $this->postJson(route(self::ROTA));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFailImageTypeValues()
    {
        $data = $this->correctValues();
        Storage::fake('local');
        $pdfFile = UploadedFile::fake()->create('file.pdf');
        $data['path'] = $pdfFile;

        $response = $this->actingAs($this->user)->postJson(route(self::ROTA), $data);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'path',
                ],
            ]);
    }

    public function testFailDateFormatValues()
    {
        $data = $this->correctValues();
        $data['birth_date'] = Carbon::now()->format('Y-m-d');

        $response = $this->actingAs($this->user)->postJson(route(self::ROTA), $data);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'birth_date',
                ],
            ]);
    }

    public function testFailMaxLenghtValues()
    {
        $data = $this->correctValues();
        $bigValues = str_pad('', 101, 'A');
        $data['name'] = $bigValues;
        $data['mother_name'] = $bigValues;
        $data['cpf'] = $bigValues;
        $data['cns'] = $bigValues;
        $data['street'] = $bigValues;
        $data['district'] = $bigValues;
        $data['city'] = $bigValues;
        $data['state'] = $bigValues;
        $data['post_code'] = $bigValues;
        $data['complement'] = $bigValues;

        $response = $this->actingAs($this->user)->postJson(route(self::ROTA), $data);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'name',
                    'mother_name',
                    'cpf',
                    'cns',
                    'district',
                    'street',
                    'city',
                    'state',
                    'post_code',
                    'complement',
                ],
            ]);
    }

    public function testFailRequiredValues()
    {
        $emptyValue = [];

        $response = $this->actingAs($this->user)->postJson(route(self::ROTA), $emptyValue);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'name',
                    'mother_name',
                    'birth_date',
                    'cpf',
                    'cns',
                    'district',
                    'street',
                    'city',
                    'state',
                    'post_code',
                    'number',
                ],
            ]);
    }

    public function testFailTypeValues()
    {
        $intValue = 1;
        $stringValue = 'a';
        $loginData = [
            'name' => $intValue,
            'mother_name' => $intValue,
            'birth_date' => $intValue,
            'cpf' => $stringValue,
            'cns' => $stringValue,
            'district' => $intValue,
            'street' => $intValue,
            'city' => $intValue,
            'state' => $intValue,
            'post_code' => $stringValue,
            'complement' => $intValue,
            'number' => $stringValue,
            'path' => $intValue
        ];

        $response = $this->actingAs($this->user)->postJson(route(self::ROTA), $loginData);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'name',
                    'mother_name',
                    'birth_date',
                    'cpf',
                    'cns',
                    'district',
                    'street',
                    'city',
                    'state',
                    'post_code',
                    'complement',
                    'number',
                    'path'
                ],
            ]);
    }

    public function testSucess()
    {
        $data = $this->correctValues();

        $response = $this->actingAs($this->user)->postJson(route(self::ROTA), $data);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'message',
            ]);
    }

    private function correctValues()
    {
        Storage::fake('local');
        $imagem = UploadedFile::fake()->create('imagem.png');
        $patient = Patient::factory()->make()->toArray();
        $address = Address::factory()->make()->toArray();
        return array_merge($patient, $address, ['path' => $imagem]);
    }
}
